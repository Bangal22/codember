fn main() {
    let input: String = "&###@&*&###@@##@##&######@@#####@#@#@#@##@@@@@@@@@@@@@@@*&&@@@@@@@@@####@@@@@@@@@#########&#&##@@##@@##@@##@@##@@##@@##@@##@@##@@##@@##@@##@@##@@##@@##@@&".to_string();
    input.split("").fold(0, |mut acc: i32, operation: &str| {
        match operation {
            "#" => acc += 1,
            "@" => acc -= 1,
            "*" => acc *= acc,
            "&" => print!("{:?}", acc),
            _ => (),
        }
        acc
    });
}