const Operation = {
    Increase: "#",
    Decrease: "@",
    Multiple: "*",
    Print: "&"
}

function compile (input) {
    let output = ""
    input.split("").reduce((acc, operation) => {
        if (Operation.Increase == operation) acc += 1 
        else if (Operation.Decrease == operation)  acc -= 1
        else if (Operation.Multiple == operation) acc *= acc
        else if (Operation.Print == operation) output += acc
        return acc
    }, 0)
    return output
}

function main() {
    const input = "&###@&*&###@@##@##&######@@#####@#@#@#@##@@@@@@@@@@@@@@@*&&@@@@@@@@@####@@@@@@@@@#########&#&##@@##@@##@@##@@##@@##@@##@@##@@##@@##@@##@@##@@##@@##@@##@@&"
    let output = compile(input);
    console.log(output)
}

main()